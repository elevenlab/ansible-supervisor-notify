Ansible Role: Supervisor-Notify
===================
Install and configure supervisor-notify script to send notification

The command should be added to the `notify_fatal` configuration in supervisor (`php /path/to/library/src/notify.php`). see [supervisor role](https://bitbucket.org/elevenlab/ansible-supervisor)

----------
Requirment
--------------

- user owning `supervisor-notify` folder and script must exist on the system (by defualt `supervisor`). See [common role](https://bitbucket.org/elevenlab/ansible-common) to add user
- user owning `supervisor-notify` folder and script must have the privileges to pull from repository (i.e. he must have the ssh key in the .ssh folder). See [sshkey role](https://bitbucket.org/elevenlab/ansible-sshkey)
- composer must be installed on the target system. See [composer role](https://bitbucket.org/elevenlab/ansible-composer)
- as a requirment of composer and in order to allow supervisord to execute the command php must be installed on the system. See [php-fpm role](https://bitbucket.org/elevenlab/ansible-php-fpm)

Role variables
-------------
Available variables are listed below, along with default values (see defaults/main.yml):

	s_notify_user: supervisor
This is the user who own the files and directory of the `supervisor-notify` script. It is also the user who pull form git and execute composer install
	
	s_notify_repo: git@bitbucket.org:elevenlab/supervisor-notify.git
This is the repository where the `supervisor-notify` is located

	s_notify_dir: /var/www/supervisor-notify
This is the folder where the repository is cloned in the target machine

	s_notfy_debug: false
    s_notfy_mail_host: ssl0.ovh.net
    s_notfy_mail_port: 465
    s_notfy_mail_connection: ssl

    s_notfy_mail_username: 
    s_notfy_mail_password: 

    s_notfy_mail_from_name:
    s_notfy_mail_from_address:

    s_notfy_mail_to_address: info@moveax.it
    s_notfy_mail_to_name: 'MovEax srls'

These are the variable needed to configure the script. It will allow the access to the mail provider to send mail from `s_notfy_mail_from_address` to `s_notfy_mail_to_address`
   
Dependencies
-------------------


Example Playbook
--------------------------

   
